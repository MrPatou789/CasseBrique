const CANVAS_WITDH = 1024
const CANVAS_HEIGHT = 720
class MOUVEMENT {
    constructor(x, y){
        this._x = x
        this._y = y
    }

    get x (){
        return this._x
    }

    get y (){
        return this._y
    }

    set x (x){
        this._x = x
    }
    set y (y){
        this._y = y
    }
}

class BRIQUE extends MOUVEMENT{
    constructor (x, y, width, height, life, newFunction) {
        super (x, y)
        this._life = life // Une vie de 4 est une brique incassable
        this._colorList = ["#066100", "#a85400", "#820404", "#453d3d"]
        this._color = this._colorList[life - 1]
        this._width = width
        this._height = height
        this._onDestruction = newFunction || function(){}
    }

    get life () {
        return this._life
    }

    get color () {
        return this._color
    }

    get width () {
        return this._width
    }

    get height () {
        return this._height
    }

    set onDestruction (newFunction) {
        return this._onDestruction = newFunction
    }

    get onDestruction(){
        this._onDestruction()
    }

    syncColor () {
        this._color = this._colorList[this._life - 1]
    }

    lostLife () {
        if (this._life !== 4) {
            this._life -= 1
            if(this._life === 0) {
                this.onDestruction
                return true
            }
            this.syncColor()
        }
        return false
    }
}

class BARRE extends MOUVEMENT {
    constructor(x, y, width, height) {
        super(x, y)
        this._life = 3
        this._width = width
        this._height = height
    }

    get life () {
        return this._life
    }

    get width() {
        return this._width
    }

    get height() {
        return this._height
    }

    set width (width) {
        this._width = width
    }

    lostLife () {
        this._life -= 1
        if(this._life === 0) return false
        return true
    }
}

class BALLE extends MOUVEMENT {
    constructor(x, y , dx , dy){
        super(x, y)
        this._dx = dx
        this._dy = dy
    }

    get dx () {
        return this._dx
    }

    get dy () {
        return this._dy
    }

    set dx (dx) {
        this._dx = dx
    }

    set dy (dy) {
        this._dy = dy
    }
}

class MISSILE extends MOUVEMENT {
    constructor(x, y){
        super(x, y)
        const image = new Image()
        image.src = `${location.pathname}/../assets/svg/torpedo.svg`
        this._svg = image
    }

    get svg () {
        return this._svg
    }
}

class BOMBE extends MOUVEMENT {
    constructor(x, y){
        super(x, y)
        const image = new Image()
        image.src = `${location.pathname}/../assets/svg/bomb.svg`
        this._svg = image
    }

    get svg () {
        return this._svg
    }
}

class OBJET extends MOUVEMENT {
    constructor(x, y, objectFunction, svg){
        super(x, y)
        this._makeEffect = objectFunction
        this._svg = svg
    }

    makeEffect () {
        this._makeEffect()
    }

    get svg () {
        return this._svg
    }
}

var M = {
    briques: [],
    barre: null,
    missiles: [],
    bombes: [],
    balles: [],
    objets: [],
    points : 0,
    levels: {
        1 : [
            [4,4,4,4,4,4,4,4,4,4,4,4,4,4],
            [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
            [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
            [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
            [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
            [1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        ],
        2 : [
            [4,4,4,4,2,2,1,1,2,2,4,4,4,4],
            [2,2,2,2,3,3,0,0,3,3,2,2,2,2],
            [2,2,2,3,3,0,0,0,0,3,3,2,2,2],
            [2,2,2,2,2,0,0,0,0,2,2,1,2,2],
            [3,3,2,2,0,0,0,0,0,0,2,2,3,3],
            [1,1,0,0,0,0,0,0,0,0,0,0,1,1]
        ],
        3 : [
            [3,2,0,0,0,0,0,0,0,0,0,0,2,3],
            [3,2,1,0,0,0,0,0,0,0,0,1,2,3],
            [3,2,1,2,0,0,0,0,0,0,2,1,2,3],
            [3,2,1,2,3,0,0,0,0,3,2,1,2,3],
            [3,2,1,2,3,1,0,0,1,3,2,1,2,3],
            [4,4,4,4,2,2,1,1,2,2,4,4,4,4]
        ],
        4 : [            
            [4,4,1,1,1,0,0,0,0,1,1,1,4,4],
            [0,0,0,3,3,4,2,2,4,3,3,0,0,0],
            [1,2,3,1,1,2,3,3,2,1,1,3,2,1],
            [2,1,3,1,1,2,3,3,2,1,1,3,1,2],
            [0,0,0,3,3,4,2,2,4,3,3,0,0,0],
            [4,4,1,1,1,0,0,0,0,1,1,1,4,4]

        ],
        5 : [
            [2,0,0,0,1,1,2,2,1,1,0,0,0,2],
            [2,2,0,0,0,3,2,2,3,0,0,0,2,2],
            [1,2,2,3,4,4,3,3,4,4,3,2,2,1],
            [1,2,2,3,4,4,3,3,4,4,3,2,2,1],
            [2,2,0,0,0,0,0,0,0,0,0,0,2,2],
            [2,0,0,0,0,0,0,0,0,0,0,0,0,2]
        ],
        6 : [
            [3,0,0,0,0,3,2,2,3,0,0,0,0,3],
            [3,3,0,0,0,2,3,3,2,0,0,0,3,3],
            [2,3,3,0,0,2,3,3,2,0,0,3,3,2],
            [2,2,3,3,4,3,0,0,3,4,3,3,0,2],
            [2,2,2,3,4,4,4,4,4,4,3,2,2,2],
            [2,2,2,3,2,1,1,1,1,2,3,2,2,2]
        ],
    },
    levelsProbalility : {
        1 :  {
            bomb : [0, 10],
            barWidthDividedBy2 : [0, 0],
            reverseDirection : [0,0],
            barSlower : [0, 0],
            ballAccelerator : [0, 0],
            barWidthBy2 : [10, 20],
            missile : [0, 0],
            ball : [0, 0],
            barreAccelerator : [0,0]
        },
        2 : {
            bomb : [0, 10],
            barWidthDividedBy2 : [10, 15],
            reverseDirection : [0,0],
            barSlower : [0, 0],
            ballAccelerator : [0, 0],
            barWidthBy2 : [15, 20],
            missile : [0, 0],
            ball : [20, 25],
            barreAccelerator : [0,0]
        },
        3 : {
            bomb : [0, 10],
            barWidthDividedBy2 : [10, 15],
            reverseDirection : [15, 20],
            barSlower : [0, 0],
            ballAccelerator : [0, 0],
            barWidthBy2 : [20, 25],
            missile : [0, 0],
            ball : [25, 30],
            barreAccelerator : [30,35]
        },
        4 : {
            bomb : [0, 10],
            barWidthDividedBy2 : [10, 15],
            reverseDirection : [15, 20],
            barSlower : [0, 0],
            ballAccelerator : [0, 0],
            barWidthBy2 : [20, 25],
            missile : [0, 0],
            ball : [25, 30],
            barreAccelerator : [30,35]
        },
        5 : {
            bomb : [0, 10],
            barWidthDividedBy2 : [10, 12],
            reverseDirection : [12, 15],
            barSlower : [15, 19],
            ballAccelerator : [19, 21],
            barWidthBy2 : [21, 24],
            missile : [24, 27],
            ball : [27, 30],
            barreAccelerator : [30,32]
        },
        6 : {
            bomb : [0, 10],
            barWidthDividedBy2 : [10, 15],
            reverseDirection : [15, 20],
            barSlower : [20, 25],
            ballAccelerator : [25, 30],
            barWidthBy2 : [0, 0],
            missile : [30, 32],
            ball : [32, 34],
            barreAccelerator : [0, 0]
        },
    },
    actualLevel: 0
}

var V = {
    
    defineKeyUpEventListener (){
        document.removeEventListener("keydown", C.postInit)
        document.addEventListener('keydown', e => {
            if (e.key === 'ArrowRight' || e.key === 'ArrowLeft') {
                C.setBarre(e.key)
            }
        }) 
    },

    step (){
        requestAnimationFrame(function () {
            const ctx = document.getElementById('game').getContext('2d')
            ctx.globalCompositeOperation = 'destination-over'
            ctx.canvas.width = CANVAS_WITDH
            ctx.canvas.height = CANVAS_HEIGHT
            ctx.clearRect(0, 0, ctx.witdh, ctx.height)

            V.drawBarre(ctx)
            V.drawBriques(ctx)
            V.drawBalles(ctx)
            V.drawBombes(ctx)
            V.drawObject(ctx)
            V.drawMissile(ctx)

            C.setBalles()
            C.setBombes()
            C.setObject()
            C.setMissile()

            if(!C.getCancelAnimationFrame()){
                V.step()
            }
        })
    },

    drawBarre (ctx) {
        const barre = C.getBarre()
        ctx.fillStyle = "#000000"
        ctx.fillRect(barre.x, barre.y, barre.width , barre.height)
        ctx.font = "1rem Arial"
        ctx.fillText(`Points : ${C.getPoints()}`, 20, CANVAS_HEIGHT - 10)

        for (let i = 0; i < barre.life; i++) {
            ctx.beginPath()
            ctx.arc(CANVAS_WITDH - 10 - (i * 15), CANVAS_HEIGHT - 20, C.BALLE_RADIUS, 0, 2 * Math.PI)
            ctx.fillStyle = "red"
            ctx.fill()
            ctx.strokeStyle = "white"
            ctx.stroke()
        }
    },

    drawBriques(ctx) {
        const briques = C.getBriques()
        briques.forEach(brique => {
            ctx.fillStyle = brique.color
            ctx.fillRect(brique.x, brique.y, brique.width , brique.height)
        })
    },
    
    drawBalles(ctx) {
        const balles = C.getBalles()
        balles.forEach(balle => {
            ctx.beginPath()
            ctx.arc(balle.x, balle.y, C.BALLE_RADIUS, 0, 2 * Math.PI, true)
            ctx.fillStyle = "white"
            ctx.fill()
            ctx.strokeStyle = "white"
            ctx.stroke()
        })
    },

    drawBombes(ctx) {
     M.bombes.forEach(bombe => {
        ctx.drawImage(bombe.svg, bombe.x, bombe.y, C.OBJECT_SIZE, C.OBJECT_SIZE);
     });
    },

    drawObject(ctx) {
        M.objets.forEach(object => {
            ctx.drawImage(object.svg, object.x, object.y, C.OBJECT_SIZE, C.OBJECT_SIZE);
         }); 
    },

    drawMissile(ctx) {
        M.missiles.forEach(missile => {
            ctx.drawImage(missile.svg, missile.x, missile.y, C.OBJECT_SIZE, C.OBJECT_SIZE)
        })
    },

    nextLevel() {
        const element = document.getElementById("game-level")
        element.children[0].innerHTML = `Level ${C.getActualLevel()} <br/> Points : ${C.getPoints()}`
        if(element.style.display === "none") element.style.display = "block"
        else element.style.display = "none"
    },

    afterGame(msg) {
        const element = document.getElementById("game-after")
        element.style.display = "block"
        element.children[0].innerHTML = `${msg} <br/> Points : ${C.getPoints()}` 
    }
}

var C = {
    BARRE_WIDTH: 150,
    BARRE_HEIGHT: 7,
    BARRE_SPEED: 20,
    BRIQUE_WIDTH: 70,
    BRIQUE_HEIGHT: 30,
    BALLE_SPEED : -5,
    BALLE_RADIUS : 6,
    OBJECT_SIZE : 15,
    cancelAnimationFrame: false,
    reverseDirection : false,
    
    postInit(e) {
        C.init()
        document.getElementById("game-before").style.display = "none"
    },

    init() {
        C.nextLevel()
        V.defineKeyUpEventListener()
        V.step()
    },

    getBarre() {
        return M.barre
    },
    
    getMissiles() {
        return M.missiles
    },

    getBriques() {
        return M.briques
    },

    getBombes() {
        return M.bombes
    },

    getObjets() {
        return M.objets
    },

    getBalles() {
        return M.balles
    },

    getLevels() {
        return M.levels
    },

    getActualLevel() {
        return M.actualLevel
    },

    getCancelAnimationFrame() {
        return C.cancelAnimationFrameBoolean
    },

    getPoints() {
        return M.points
    },

    setActualLevel() {
     M.actualLevel = level++
    },

    setBarre(key) {
        const barre = M.barre
        if(C.reverseDirection && key === "ArrowRight") key = "ArrowLeft"
        else if(C.reverseDirection && key === "ArrowLeft") key = "ArrowRight"

        if (key === "ArrowRight") {
            if (barre.x + C.BARRE_SPEED < CANVAS_WITDH - barre.width) {
                barre.x += C.BARRE_SPEED
            }
        } else {
            if (barre.x - C.BARRE_SPEED > 0) {
                barre.x -= C.BARRE_SPEED
            }
        }
    },

    setLife(balle){
        if(balle){
            M.balles = M.balles.filter(element => element != balle)
            if(M.balles.length >= 1) return 
        }
        if(M.barre.lostLife()){
            if(balle) M.balles.push(new BALLE(CANVAS_WITDH / 2, CANVAS_HEIGHT - (40 + C.BALLE_RADIUS), C.getRandom(-4, 4), C.BALLE_SPEED))
        }else{
            C.afterGame("Partie perdu")
        }
    },

    setBalles() {
        M.balles.forEach(balle => {
            const barre = M.barre
            let dx = balle.dx
            let dy = balle.dy

            if(balle.y > CANVAS_HEIGHT - 10 ) {
                C.setLife(balle)
                return
            }
            if(balle.x + dx > CANVAS_WITDH - C.BALLE_RADIUS || balle.x + dx < C.BALLE_RADIUS) dx = -dx
            if(balle.y + dy > CANVAS_HEIGHT - C.BALLE_RADIUS || balle.y + dy < C.BALLE_RADIUS) dy = -dy
            if(balle.y + dy >= barre.y && balle.y + dy <= (barre.y + barre.height) && balle.x >= barre.x + dx && balle.x + dx <= (barre.x + barre.width)){
                dx = Math.floor(8 * ((balle.x - (barre.x + barre.width /2 )) / barre.width))
                dy = -dy
            } 
            M.briques = M.briques.filter(brique => {
                if(balle.y + dy >= brique.y && balle.y + dy <= (brique.y + C.BRIQUE_HEIGHT) && balle.x + dx >= brique.x && balle.x + dx <= (brique.x + C.BRIQUE_WIDTH)){
                    dy = -dy
                    if(brique.life != 4) M.points += 10
                    if(brique.lostLife()) return false
                    return true
                }
                return true
            });
            balle.x += dx
            balle.y += dy
            balle.dx = dx
            balle.dy = dy
            if(M.briques.every(brique => brique.life === 4)) C.nextLevel()
        })
    },

    setBombes() {
        const barre = M.barre
        M.bombes = M.bombes.filter(bombe => {
            bombe.y += 2
            if(bombe.y > CANVAS_HEIGHT) return false
            if(bombe.y + C.OBJECT_SIZE >= barre.y && bombe.y + C.OBJECT_SIZE <= (barre.y + C.BARRE_HEIGHT) && bombe.x >= barre.x && bombe.x <= (barre.x + C.BARRE_WIDTH)){
                C.setLife()
                return false
            }
            return true
        });
    },

    setMissile() {
        const barre = M.barre
        let save = true
        M.missiles = M.missiles.filter(missile => {
            missile.y -= 4
            if(missile.y < 0) return false
            save = true
            M.briques = M.briques.filter(brique => {
                if(missile.y >= brique.y && missile.y + C.OBJECT_SIZE <= (brique.y + C.BRIQUE_HEIGHT) && missile.x >= brique.x && missile.x + C.OBJECT_SIZE <= (brique.x + C.BRIQUE_WIDTH)) {
                    save = false
                    if(brique.lostLife()) return false
                }
                return true
            })
            return save
        })
    },

    setObject () {
        const barre = M.barre
        M.objets = M.objets.filter(object => {
            object.y += 2
            if(object.y > CANVAS_HEIGHT) return false
            if(object.y + C.OBJECT_SIZE >= barre.y && object.y + C.OBJECT_SIZE <= (barre.y + barre.height) && object.x >= barre.x && object.x <= (barre.x + barre.width)){
                object.makeEffect()
                return false
            }
            return true
        });
    },

    setCancelAnimationFrame() {
        C.cancelAnimationFrameBoolean = !C.cancelAnimationFrameBoolean
    },

    nextLevel() {
        M.actualLevel++
        if(M.actualLevel > Object.keys(M.levels).length){
            C.afterGame("Partie gagné")
            return
        }
        M.balles = []
        M.objets = []
        M.bombes = []
        M.missiles = []
        M.briques = []
        V.nextLevel()

        if(M.barre){
            M.barre.x = (CANVAS_WITDH / 2) - (C.BARRE_WIDTH / 2)  
        }else{
            M.barre = M.barre = new BARRE((CANVAS_WITDH / 2) - (C.BARRE_WIDTH / 2)  , CANVAS_HEIGHT - 40, C.BARRE_WIDTH, C.BARRE_HEIGHT)
        }

        setTimeout(function(){
            M.balles.push(new BALLE(CANVAS_WITDH / 2, CANVAS_HEIGHT - (40 + C.BALLE_RADIUS) , C.getRandom(-4, 4), C.BALLE_SPEED))

            let positionX = 10
            let positionY = 50
    
            M.levels[M.actualLevel].forEach(row => {
                row.forEach(briqueLife => {
                    if(briqueLife !== 0){
                        M.briques.push(new BRIQUE(positionX, positionY, C.BRIQUE_WIDTH, C.BRIQUE_HEIGHT, briqueLife, C.defineObject()))
                    }
                    positionX += C.BRIQUE_WIDTH + 2
                })
                positionX = 10
                positionY += C.BRIQUE_HEIGHT + 2
            })
            V.nextLevel()
        }, 1500)
    },

    getRandom(min, max) {
        return Math.floor(Math.random() * ((max + 1) - min) + min)
    },

    afterGame(msg) {
        C.setCancelAnimationFrame()
        V.afterGame(msg)
    },

    defineObject() {
        const random = C.getRandom(0, 100)
        let object = null
        Object.keys(M.levelsProbalility[M.actualLevel]).forEach(objectKey => {
            const objectProbability = M.levelsProbalility[M.actualLevel][objectKey]
            if(random >= objectProbability[0] && random <= objectProbability[1]) object = objectKey
        })

        let objectFunction = null
        let svg = new Image()

        switch (object) {
            case "bomb":
                return function() {
                    M.bombes.push(new BOMBE(this._x + (C.BRIQUE_WIDTH / 2) - 10, this._y))
                }

            case "barWidthDividedBy2":
                objectFunction = function() {
                    const barre = M.barre
                    barre.width /= 2
                    setTimeout(function() {
                        barre.width *= 2
                    }, 20000)
                }
                svg.src = `${location.pathname}/../assets/svg/resize-option.svg`
                return function(){
                    M.objets.push(new OBJET(this._x + (C.BRIQUE_WIDTH / 2)- 10, this._y, objectFunction, svg))
                }

            case "barSlower":
                objectFunction = function() {
                    C.BARRE_SPEED -= 5
                    setTimeout(function (){
                        C.BARRE_SPEED += 5
                    }, 30000)
                }
                svg.src = `${location.pathname}/../assets/svg/snail.svg`
                return function(){
                    M.objets.push(new OBJET(this._x + (C.BRIQUE_WIDTH / 2)- 10, this._y, objectFunction, svg))
                }

            case "reverseDirection" :
                objectFunction = function() {
                    C.reverseDirection = true
                    setTimeout(function (){
                        C.reverseDirection = false
                    }, 30000)
                }
                svg.src = `${location.pathname}/../assets/svg/refresh.svg` 
                return function(){
                    M.objets.push(new OBJET(this._x + (C.BRIQUE_WIDTH / 2)- 10, this._y, objectFunction, svg))
                }

            case "barWidthBy2":
                objectFunction = function() {
                    const barre = M.barre
                    barre.width *= 2
                    setTimeout(function() {
                        barre.width /= 2
                    }, 20000)
                }
                svg.src = `${location.pathname}/../assets/svg/increase-size-option.svg`
                return function(){
                    M.objets.push(new OBJET(this._x + (C.BRIQUE_WIDTH / 2)- 10, this._y, objectFunction, svg))
                }

            case "ball":
                objectFunction = function() {
                    M.balles.push(new BALLE(CANVAS_WITDH / 2, CANVAS_HEIGHT - (40 + C.BALLE_RADIUS) , C.getRandom(-4, 4), C.BALLE_SPEED))
                    M.balles.push(new BALLE(CANVAS_WITDH / 2, CANVAS_HEIGHT - (40 + C.BALLE_RADIUS) , C.getRandom(-4, 4), C.BALLE_SPEED))
                }
                svg.src = `${location.pathname}/../assets/svg/ammunition.svg`
                return function(){
                    M.objets.push(new OBJET(this._x + (C.BRIQUE_WIDTH / 2)- 10, this._y, objectFunction, svg))
                }

            case "barreAccelerator":
                objectFunction = function() {
                    C.BARRE_SPEED += 10
                    setTimeout(function (){
                        C.BARRE_SPEED -= 10
                    }, 30000)
                }
                svg.src = `${location.pathname}/../assets/svg/speed.svg`
                return function(){
                    M.objets.push(new OBJET(this._x + (C.BRIQUE_WIDTH / 2)- 10, this._y, objectFunction, svg))
                }

            case "ballAccelerator":
                objectFunction = function() {
                    const balles = M.balles
                    balles.forEach(balle => {
                        balle.dy *= 2
                    })
                    setTimeout(function (){
                        const balles = M.balles
                        balles.forEach(balle => {
                            if(balle.dy != C.BALLE_SPEED){
                                balle.dy /= 2
                            }
                        })
                    }, 30000)
                }
                svg.src = `${location.pathname}/../assets/svg/sports-and-competition.svg`
                return function(){
                    M.objets.push(new OBJET(this._x + (C.BRIQUE_WIDTH / 2)- 10, this._y, objectFunction, svg))
                }
                
            case "missile":
                objectFunction = function() {
                    function addMissile(){
                        const barre = M.barre
                        M.missiles.push(new MISSILE(barre.x + (barre.width / 2), barre.y - (C.BRIQUE_HEIGHT / 2)))
                    }
                    const interval = setInterval(function(){
                        addMissile()
                    },500)
                    setTimeout(function(){
                        clearInterval(interval)
                    }, 30000)
                }
                svg.src = `${location.pathname}/../assets/svg/torpedo45.svg`
                return function() {
                    M.objets.push(new OBJET(this._x + (C.BRIQUE_WIDTH / 2) - 10, this._y, objectFunction, svg))
                }

            default:
                return null
        }
    }
}

document.addEventListener('keydown', C.postInit)
